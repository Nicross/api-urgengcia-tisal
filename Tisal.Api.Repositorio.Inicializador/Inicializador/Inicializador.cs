﻿using System.ComponentModel.Composition;
using Tisal.Api.Repositorio.Core;
using Tisal.NCommon.ComponentModel;

namespace Tisal.Api.Repositorio.Inicializador
{
    /// <summary>
    /// Inicializa una instancia del repositorio con la interfaz
    /// </summary>
    [Export(typeof(IModulo))]
    public class Inicializador : IModulo
    {
        /// <summary>
        /// Método para inicializar repositorios.
        /// </summary>
        /// <param name="registrar">Interfaz de RegistraModulo.</param>
        public void Initialize(IRegistraModulo registrar)
        {
            registrar.Registrar<ITriageRepositorio, TriageRepositorio>();
        }
    }
}