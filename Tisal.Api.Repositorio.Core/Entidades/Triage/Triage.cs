﻿using System;

namespace Tisal.Api.Repositorio.Core
{
    /// <summary>
    /// Clase que representa a Triage.
    /// </summary>
    public class Triage
    {
        /// <summary>
        /// Obtiene o asigna Fecha de inicio Triage.
        /// </summary>
        public DateTime FechaInicioTriage { get; set; }

        /// <summary>
        /// Obtiene o asigna evaluación.
        /// </summary>
        public int Evaluacion { get; set; }

        /// <summary>
        /// Obtiene o asigna Código de clasificación.
        /// </summary>
        public string CodigoClasificacion { get; set; }

        /// <summary>
        /// Obtiene o asigna Motivo de consulta.
        /// </summary>
        public string MotivoConsulta { get; set; }

        /// <summary>
        /// Obtiene o asigna Descripción de alergia.
        /// </summary>
        public string DescripcionAlergia { get; set; }

        /// <summary>
        /// Obtiene o asigna Descripcion de antecedente.
        /// </summary>
        public string DescripcionAntecedente { get; set; }

        /// <summary>
        /// Obtiene o asigna Abreviatura.
        /// </summary>
        public string Abreviatura { get; set; }

        /// <summary>
        /// Obtiene o asigna Apellido paterno.
        /// </summary>
        public string ApellidoPaterno { get; set; }

        /// <summary>
        /// Obtiene o asigna Apellido materno.
        /// </summary>
        public string ApellidoMaterno { get; set; }

        /// <summary>
        /// Obtiene o asigna Nombre.
        /// </summary>
        public string Nombre { get; set; }
    }
}
