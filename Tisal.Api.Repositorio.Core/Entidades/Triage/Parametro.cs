﻿using System;

namespace Tisal.Api.Repositorio.Core
{
    /// <summary>
    /// Representa a Parámetro.
    /// </summary>
    public class Parametro
    {
        // <summary>
        /// Obtiene o asigna la propiedad Código de grupo.
        /// </summary>
        public int CodigoGrupo { get; set; }

        /// <summary>
        /// Obtiene o asigna la propiedad Código de ítem.
        /// </summary>
        public int CodigoItem { get; set; }

        /// <summary>
        /// Obtiene o asigna la propiedad Código de ítem 2.
        /// Configuración complementaria del grupo e ítem
        /// </summary>
        public string CodigoItem2 { get; set; }

        /// <summary>
        /// Obtiene o asigna la propiedad Descripción de ítem.
        /// </summary>
        public string DescripcionItem { get; set; }

        /// <summary>
        /// Obtiene o asigna la propiedad Sub Grupo.
        /// Configuración complementaria del grupo e ítem
        /// </summary>
        public int SubGrupo { get; set; }

        /// <summary>
        /// Obtiene o asigna la propiedad Sub ítem.
        /// Configuración complementaria del grupo e ítem
        /// </summary>
        public int SubItem { get; set; }

        /// <summary>
        /// Obtiene o asigna la propiedad Orden.
        /// Permite definir el orden de los ítems del grupo e ítem
        /// </summary>
        public int Orden { get; set; }

        /// <summary>
        /// Obtiene o asigna la propiedad Fecha de próxima observación.
        /// </summary>
        public DateTime FechaProximaObservacion { get; set; }

        /// <summary>
        /// Obtiene o asigna la propiedad Texto a desplegar.
        /// Descripción del grupo o ítem
        /// Es una alternativa al campo descripción item
        /// </summary>
        public string TextoDesplegar { get; set; }

        /// <summary>
        /// Obtiene o asigna la propiedad Url.
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// Obtiene o asigna la propiedad Código de dosis Medisyn.
        /// </summary>
        public int CodigoDosisMedisyn { get; set; }

        /// <summary>
        /// Obtiene o asigna la propiedad Código vía Medisyn.
        /// </summary>
        public int CodigoViaMedisyn { get; set; }

        /// <summary>
        /// Obtiene o asigna la propiedad Uso código ítem 2.
        /// </summary>
        public string UsoCodigoItem2 { get; set; }

        /// <summary>
        /// Obtiene o asigna la propiedad Imprimir por Ip.
        /// </summary>
        public string ImprimirPorIp { get; set; }

        /// <summary>
        /// Obtiene o asigna la propiedad Código de impresora.
        /// </summary>
        public string CodigoImpresora { get; set; }
    }
}
