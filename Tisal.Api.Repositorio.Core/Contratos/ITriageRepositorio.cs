﻿using System.Collections.Generic;

namespace Tisal.Api.Repositorio.Core
{
    /// <summary>
    /// Definición del contrato de repositorio.
    /// </summary>
    public interface ITriageRepositorio
    {
        /// <summary>
        /// Método que controla la transacción al agregar Triage.
        /// </summary>
        /// <param name="pParametros">Objeto de parámetros con las propiedades necesarias para la solicitud.</param>
        /// <returns>Retorna éxito de operación.</returns>
        bool AddTriageTrx(ParametroAddTriage pParametros);

        /// <summary>
        /// Método que obtiene Triage.
        /// </summary>
        /// <param name="pParametros">Objeto de parámetros con las propiedades necesarias para la solicitud.</param>
        /// <returns>Retorna listado de objetos triage.</returns>
        IEnumerable<Triage> GetTriage(ParametroGetTriage pParametros);

        /// <summary>
        /// Método que obtiene el cédigo de item 2 del parámetro.
        /// </summary>
        /// <param name="pParametros">Objeto de parámetros con las propiedades necesarias para la solicitud.</param>
        /// <returns>Retorna strign con el código de item 2 del parámetro</returns>
        string GetCodigoItemDosParametro(ParametroGetParametro pParametros);
    }
}
