﻿using System;
using System.Collections.Generic;

namespace Tisal.Api.Repositorio.Core
{
    /// <summary>
    /// Clase que representa las propiedades necesarias para la solicitud.
    /// </summary>
    public class ParametroAddTriage
    {
        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public ParametroAddTriage()
        {
            ListadoParametroClinico = new List<ParametroParametroClinico>();
        }

        /// <summary>
        /// Obtiene o asigna Código de empresa.
        /// </summary>
        public int? CodigoEmpresa { get; set; }

        /// <summary>
        /// Obtiene o asigna Código de sucursal.
        /// </summary>
        public int? CodigoSucursal { get; set; }

        /// <summary>
        /// Obtiene o asigna Código de origen.
        /// </summary>
        public int? CodigoOrigen { get; set; }

        /// <summary>
        /// Obtiene o asigna Código de sección.
        /// </summary>
        public int? CodigoSeccion { get; set; }

        /// <summary>
        /// Obtiene o asigna POrigen Data.
        /// </summary>
        public string OrigenData { get; set; }

        /// <summary>
        /// Obtiene o asigna Id de atención.
        /// </summary>
        public int IdAtencion { get; set; }

        /// <summary>
        /// Obtiene o asigna Id atención Lyra.
        /// </summary>
        public int? IdAtencionLyra { get; set; }

        /// <summary>
        /// Obtiene o asigna Id usuario.
        /// </summary>
        public string IdUsuario { get; set; }

        /// <summary>
        /// Obtiene o asigna Código de clasificación.
        /// </summary>
        public int? CodigoClasificacion { get; set; }

        /// <summary>
        /// Obtiene o asigna Motivo de consulta.
        /// </summary>
        public string MotivoConsulta { get; set; }

        /// <summary>
        /// Obtiene o asigna Descripción de alergia.
        /// </summary>
        public string DescripcionAlergia { get; set; }

        /// <summary>
        /// Obtiene o asigna Descripción de antecedente.
        /// </summary>
        public string DescripcionAntecendente { get; set; }

        /// <summary>
        /// Obtiene o asigna Fehca inicio Triage.
        /// </summary>
        public DateTime FechaInicioTriage { get; set; }

        /// <summary>
        /// Vigencia de la atención de Triage
        /// </summary>
        public string Vigencia { get; set; }

        /// <summary>
        /// Paramétros clínicos asociados al triage
        /// </summary>
        public List<ParametroParametroClinico> ListadoParametroClinico { get; set; }

        /// <summary>
        /// Corresponde al nombre de la tabla relacionada a la secuencia.
        /// </summary>
        public string NombreSecuencia { get; set; }

        /// <summary>
        /// Área de atención confirmada por el usuario en formulario triage.
        /// </summary>
        public string AreaAtencion { get; set; }

        /// <summary>
        /// Código de item utilizado para verificar si procede o no efectuar un update al agregar un triage
        /// </summary>
        public string CodigoItemDos { get; set; }
    }
}
