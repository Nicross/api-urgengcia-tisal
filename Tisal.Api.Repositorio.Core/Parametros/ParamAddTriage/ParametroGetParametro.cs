﻿namespace Tisal.Api.Repositorio.Core
{
    /// <summary>
    /// Objeto con los parámetros necesarios para la solicitud
    /// </summary>
    public class ParametroGetParametro
    {
        /// <summary>
        /// Campo obligatorio, 
        /// Código de empresa global conectada. 
        /// Se obtiene accediendo a la cookie DatosUsuario objeto CodigoEmpresa
        /// </summary>
        public int? CodigoEmpresa { get; set; }

        /// <summary>
        /// Campo obligatorio, 
        /// Código de sucursal global conectada.
        /// Se obtiene accediendo a la cookie Datosusuario objeto CodigoSucursal
        /// </summary>
        public int? CodigoSucursal { get; set; }

        /// <summary>
        /// Campo obligatorio, 
        /// Obtiene o asigna Código de grupo
        /// Grupo de la tabla RCE_PARAMET
        /// </summary>
        public int? CodigoGrupo { get; set; }

        /// <summary>
        /// Ítem del grupo de la tabla RCE_PARAMET
        /// Para obtener todos los ítems del grupo se debe 
        /// utilizar el valor cero como parámetro de entrada.
        /// </summary>
        public int CodigoItem { get; set; }

        /// <summary>
        /// Vigencia del parámetro.
        /// </summary>
        public string VigenciaParametro { get; set; }
    }
}