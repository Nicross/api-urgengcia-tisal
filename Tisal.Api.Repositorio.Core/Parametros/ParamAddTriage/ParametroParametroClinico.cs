﻿namespace Tisal.Api.Repositorio.Core
{
    /// <summary>
    /// Clase que representa las propiedades necesarias para la solicitud.
    /// </summary>
    public class ParametroParametroClinico
    {
        /// <summary>
        /// Código del parámetro clínico
        /// </summary>
        public int CodigoItem { get; set; }

        /// <summary>
        /// Valor del parámetro clínico.
        /// </summary>
        public string Valor { get; set; }

        /// <summary>
        /// Vigencia del registro
        /// </summary>
        public string Vigencia { get; set; }
    }
}
