﻿namespace Tisal.Api.Repositorio.Core
{
    /// <summary>
    /// Contiene los parámetros requeridos por el método GetTriage.
    /// </summary>
    public class ParametroGetTriage
    {
        /// <summary>
        /// Obtiene o asigna Código de empresa.
        /// </summary>
        public int? CodigoEmpresa { get; set; }

        /// <summary>
        /// Obtiene o asigna Código de sucursal.
        /// </summary>
        public int? CodigoSucursal { get; set; }

        /// <summary>
        /// Obtiene o asigna Código de origen.
        /// </summary>
        public int? CodigoOrigen { get; set; }

        /// <summary>
        /// Obtiene o asigna Id de atención.
        /// </summary>
        public int? IdAtencion { get; set; }

        /// <summary>
        /// Obtiene o asigna Id de atención Lyra.
        /// </summary>
        public int? IdAtencionLyra { get; set; }

        /// <summary>
        /// Obtiene o asigna la vigencia del registro de triage, Ref: S
        /// RCE_DATOS_TRIAGE
        /// </summary>
        public string VigenciaTriage { get; set; }

        /// <summary>
        /// Obtiene o asigna la vigencia del usuario, Ref: S,
        /// RCE_USUARIOS
        /// </summary>
        public string VigenciaUsuario { get; set; }
    }
}
