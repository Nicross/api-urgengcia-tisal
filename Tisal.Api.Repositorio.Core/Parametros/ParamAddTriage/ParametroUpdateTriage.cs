﻿using System;
using System.Collections.Generic;

namespace Tisal.Api.Repositorio.Core
{
    /// <summary>
    /// Clase que representa las propiedades necesarias para la solicitud.
    /// </summary>
    public class ParametroUpdateTriage
    {
        /// <summary>
        /// Categoría triage seleccionada por el usuario en formulario triage
        /// </summary>
        public int? CodigoClasificacion { get; set; }

        /// <summary>
        /// Área de atención confirmada por el usuario en formulario triage.
        /// </summary>
        public string CodigoAreaAtencion { get; set; }

        /// <summary>
        /// Obtiene o asigna Código de empresa.
        /// </summary>
        public int? CodigoEmpresa { get; set; }

        /// <summary>
        /// Obtiene o asigna Código de sucursal.
        /// </summary>
        public int? CodigoSucursal { get; set; }

        /// <summary>
        /// Obtiene o asigna el codigo de origen
        /// </summary>
        public int CodigoOrigen { get; set; }

        /// <summary>
        /// Obtiene o asigna Id usuario.
        /// </summary>
        public string IdAtencion { get; set; }

        /// <summary>
        /// Obtiene o asigna el id de atencion lyra
        /// </summary>
        public string IdAtencionLyra { get; set; }


    }
}
