﻿using AutoMapper;
using System.ComponentModel.Composition;
using Tisal.Api.Repositorio.Core;
using Tisal.Api.Servicio.Core;

namespace Tisal.Api.Servicio.Perfiles
{
    /// <summary>
    /// Genera los perfiles para mapear objeto de transporte de datos.
    /// </summary>
    [Export(typeof(Profile))]
    public class MapeoDTO : Profile
    {
        /// <summary>
        /// Configura el mapeo de entidades.
        /// </summary>
        protected void Configure()
        {
            CreateMap<AddTriageRequest, ParametroAddTriage>().ReverseMap();
            CreateMap<GetTriageRequest, ParametroGetTriage>().ReverseMap();
            CreateMap<Triage, TriageDTO>().ReverseMap();
            CreateMap<ParametroClinicoRequest, ParametroParametroClinico>().ReverseMap();
        }
    }
}