﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using Tisal.Api.Repositorio.Core;
using Tisal.Api.Servicio.Core;
using Tisal.Api.Utilitario;
using Tisal.NCommon.Excepciones;
using Tisal.NCommon.Logging;

namespace Tisal.Api.Servicio
{
    /// <summary>
    /// Implementación del contrato de servicio
    /// </summary>
    public class TriageServicio : ITriageServicio
    {
        #region Miembros

        /// <summary>
        /// Implementación del contrato de repositorio.
        /// </summary>
        private ITriageRepositorio _triageRepositorio;

        /// <summary>
        /// Representa a AutoMapper.
        /// </summary>
        private readonly IMapper _mapper;

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor predeterminado que inicializa el repositorio.
        /// </summary>
        public TriageServicio(ITriageRepositorio pPacienteRepositorio, IMapper pMapper)
        {
            _triageRepositorio = pPacienteRepositorio;
            _mapper = pMapper;
        }

        #endregion

        #region Métodos Base

        /// <summary>
        /// Método que agrega Triage.
        /// </summary>
        /// <param name="pRequest">Objeto request con los parámetros necesarios para la solicitud</param>
        /// <returns>Retorna éxito de la operación.</returns>
        public bool AddTriage(AddTriageRequest pRequest)
        {
            try
            {
                LoggingProvider.LogTraza(Generico.ObtenerPropiedadesyValoresDesdeClase(pRequest), Prioridad.Traza, string.Format("Traza: {0}", MethodBase.GetCurrentMethod().Name));

                var parametroAddTriage = _mapper.Map<ParametroAddTriage>(pRequest);
                parametroAddTriage.CodigoItemDos = GetCodigoItemDosParametro(pRequest);
                parametroAddTriage.NombreSecuencia = EnumNombreSecuencia.SEC_RCE_EVAL_EXAMFISICO.ToString();

                var respuesta = _triageRepositorio.AddTriageTrx(parametroAddTriage);

                LoggingProvider.LogTraza(string.Format("Salida: {0}", respuesta), Prioridad.Traza, string.Format("Clase: {0}, Método: {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                return respuesta;
            }
            catch (Exception ex)
            {
                if (ex.InnerException is ExcepcionNegocio || ex.InnerException is ExcepcionBaseDatos || ex.InnerException is ExcepcionAplicacion)
                {
                    throw ex.InnerException;
                }
                else
                {
                    throw new ExcepcionAplicacion(string.Format("Clase: {0}, Método: {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name), ex);
                }
            }
        }

        /// <summary>
        /// Método que obtiene Triage.
        /// </summary>
        /// <param name="pRequest">Objeto request con los parámetros necesarios para la solicitud</param>
        /// <returns>Retorna listado de objetos triage.</returns>
        public IEnumerable<TriageDTO> GetTriage(GetTriageRequest pRequest)
        {
            try
            {
                LoggingProvider.LogTraza(Generico.ObtenerPropiedadesyValoresDesdeClase(pRequest), Prioridad.Traza, string.Format("Traza: {0}", MethodBase.GetCurrentMethod().Name));

                var respuesta = _mapper.Map<IEnumerable<TriageDTO>>(_triageRepositorio.GetTriage(_mapper.Map<ParametroGetTriage>(pRequest)));

                LoggingProvider.LogTraza(string.Format("Salida: {0}", respuesta), Prioridad.Traza, string.Format("Clase: {0}, Método: {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));

                return respuesta;
            }
            catch (Exception ex)
            {
                if (ex.InnerException is ExcepcionNegocio || ex.InnerException is ExcepcionBaseDatos || ex.InnerException is ExcepcionAplicacion)
                {
                    throw ex.InnerException;
                }
                else
                {
                    throw new ExcepcionAplicacion(string.Format("Clase: {0}, Método: {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name), ex);
                }
            }
        }

        /// <summary>
        /// Método que valida los parámetros de entrada del request
        /// </summary>
        /// <param name="pRequest">Objeto request con los parámetros necesarios para la solicitud</param>
        /// <returns>Retorna string con los errores detectados</returns>
        public string ValidaAddTriage(AddTriageRequest pRequest)
        {
            try
            {
                var respuestaValidaciones = new StringBuilder();

                if (pRequest.CodigoEmpresa == null || pRequest.CodigoEmpresa < 1)
                {
                    respuestaValidaciones.Append(string.Format(MensajesNegocio.errorParametroEntradaRequest, EnumCamposValidacionRequest.CODIGO_EMPRESA.ToString(), Environment.NewLine));
                }

                if (pRequest.CodigoSucursal == null || pRequest.CodigoSucursal < 1)
                {
                    respuestaValidaciones.Append(string.Format(MensajesNegocio.errorParametroEntradaRequest, EnumCamposValidacionRequest.CODIGO_SUCURSAL.ToString(), Environment.NewLine));
                }

                if (pRequest.CodigoOrigen == null || pRequest.CodigoOrigen < 1)
                {
                    respuestaValidaciones.Append(string.Format(MensajesNegocio.errorParametroEntradaRequest, EnumCamposValidacionRequest.CODIGO_ORIGEN.ToString(), Environment.NewLine));
                }

                if (pRequest.CodigoSeccion == null || pRequest.CodigoSeccion < 1)
                {
                    respuestaValidaciones.Append(string.Format(MensajesNegocio.errorParametroEntradaRequest, EnumCamposValidacionRequest.CODIGO_SECCION.ToString(), Environment.NewLine));
                }

                if (string.IsNullOrWhiteSpace(pRequest.OrigenData))
                {
                    respuestaValidaciones.Append(string.Format(MensajesNegocio.errorParametroEntradaRequest, EnumCamposValidacionRequest.ORIGEN_DATA.ToString(), Environment.NewLine));
                }

                if (pRequest.IdAtencionLyra == null || pRequest.IdAtencionLyra < 1)
                {
                    respuestaValidaciones.Append(string.Format(MensajesNegocio.errorParametroEntradaRequest, EnumCamposValidacionRequest.ID_ATENCION_LYRA.ToString(), Environment.NewLine));
                }

                if (pRequest.CodigoClasificacion == null || pRequest.CodigoClasificacion < 1)
                {
                    respuestaValidaciones.Append(string.Format(MensajesNegocio.errorParametroEntradaRequest, EnumCamposValidacionRequest.CLASIFICACION_TRIAGE.ToString(), Environment.NewLine));
                }

                if (!pRequest.FechaInicioTriage.ValidaFecha())
                {
                    respuestaValidaciones.Append(string.Format(MensajesNegocio.errorParametroEntradaRequest, EnumCamposValidacionRequest.FECHA_INICIO_TRIAGE.ToString(), Environment.NewLine));
                }

                if (string.IsNullOrWhiteSpace(pRequest.IdUsuario))
                {
                    respuestaValidaciones.Append(string.Format(MensajesNegocio.errorParametroEntradaRequest, EnumCamposValidacionRequest.ID_USUARIO.ToString(), Environment.NewLine));
                }

                if (string.IsNullOrWhiteSpace(pRequest.Vigencia))
                {
                    respuestaValidaciones.Append(string.Format(MensajesNegocio.errorParametroEntradaRequest, EnumCamposValidacionRequest.VIGENCIA.ToString(), Environment.NewLine));
                }
                else
                {
                    if (!pRequest.Vigencia.ToUpper().Equals(EnumVigencia.S.ToString()) && !pRequest.Vigencia.ToUpper().Equals(EnumVigencia.N.ToString()))
                    {
                        respuestaValidaciones.Append(string.Format(MensajesNegocio.errorParametroEntradaRequest, EnumCamposValidacionRequest.VIGENCIA.ToString(), Environment.NewLine));
                    }
                }

                if (string.IsNullOrWhiteSpace(pRequest.AreaAtencion))
                {
                    respuestaValidaciones.Append(string.Format(MensajesNegocio.errorParametroEntradaRequest, EnumCamposValidacionRequest.AREA_ATENCION.ToString(), Environment.NewLine));
                }

                return respuestaValidaciones.ToString();
            }
            catch (Exception ex)
            {
                throw new ExcepcionNegocio(MensajesNegocio.errorValidacionCamposRequest, ex);
            }
        }

        /// <summary>
        /// Método que valida los parámetros de entrada del request
        /// </summary>
        /// <param name="pRequest">Objeto request con los parámetros necesarios para la solicitud</param>
        /// <returns>Retorna string con los errores detectados</returns>
        public string ValidaGetTriage(GetTriageRequest pRequest)
        {
            try
            {
                var respuestaValidaciones = new StringBuilder();

                if (pRequest.CodigoEmpresa == null || pRequest.CodigoEmpresa < 1)
                {
                    respuestaValidaciones.Append(string.Format(MensajesNegocio.errorParametroEntradaRequest, EnumGetTriage.COD_EMPRESA.ToString(), Environment.NewLine));
                }

                if (pRequest.CodigoSucursal == null || pRequest.CodigoSucursal < 1)
                {
                    respuestaValidaciones.Append(string.Format(MensajesNegocio.errorParametroEntradaRequest, EnumGetTriage.COD_SUCURSAL.ToString(), Environment.NewLine));
                }

                if (pRequest.CodigoOrigen == null || pRequest.CodigoOrigen < 1)
                {
                    respuestaValidaciones.Append(string.Format(MensajesNegocio.errorParametroEntradaRequest, EnumGetTriage.COD_ORIGEN.ToString(), Environment.NewLine));
                }

                if (pRequest.IdAtencionLyra == null || pRequest.IdAtencionLyra < 1)
                {
                    respuestaValidaciones.Append(string.Format(MensajesNegocio.errorParametroEntradaRequest, EnumGetTriage.ID_ATENCION_LYRA.ToString(), Environment.NewLine));
                }

                if (string.IsNullOrWhiteSpace(pRequest.VigenciaTriage))
                {
                    respuestaValidaciones.Append(string.Format(MensajesNegocio.errorParametroEntradaRequest, EnumGetTriage.VIGENCIA_TRIAGE.ToString(), Environment.NewLine));
                }
                else
                {
                    if (!pRequest.VigenciaTriage.ToUpper().Equals(EnumVigencia.S.ToString()) && !pRequest.VigenciaTriage.ToUpper().Equals(EnumVigencia.N.ToString()))
                    {
                        respuestaValidaciones.Append(string.Format(MensajesNegocio.errorParametroEntradaRequest, EnumGetTriage.VIGENCIA_TRIAGE.ToString(), Environment.NewLine));
                    }
                }

                if (string.IsNullOrWhiteSpace(pRequest.VigenciaUsuario))
                {
                    respuestaValidaciones.Append(string.Format(MensajesNegocio.errorParametroEntradaRequest, EnumGetTriage.VIGENCIA_USUARIO.ToString(), Environment.NewLine));
                }
                else
                {
                    if (!pRequest.VigenciaUsuario.ToUpper().Equals(EnumVigencia.S.ToString()) && !pRequest.VigenciaUsuario.ToUpper().Equals(EnumVigencia.N.ToString()))
                    {
                        respuestaValidaciones.Append(string.Format(MensajesNegocio.errorParametroEntradaRequest, EnumGetTriage.VIGENCIA_USUARIO.ToString(), Environment.NewLine));
                    }
                }

                return respuestaValidaciones.ToString();
            }
            catch (Exception ex)
            {
                throw new ExcepcionNegocio(MensajesNegocio.errorValidacionCamposRequest, ex);
            }
        }

        #endregion

        #region Métodos privados


        /// <summary>
        /// Método obtiene el codigo item dos de un parametro especifico para Triage.
        /// </summary>
        /// <param name="pRequest">Objeto request con los parámetros necesarios para la solicitud</param>
        /// <returns>Retorna un string que representa el codigo de item</returns>
        private string GetCodigoItemDosParametro(AddTriageRequest pRequest)
        {
            string codigoItem2 = null;

            try
            {
                LoggingProvider.LogTraza(Generico.ObtenerPropiedadesyValoresDesdeClase(pRequest), Prioridad.Traza, string.Format("Traza: {0}", MethodBase.GetCurrentMethod().Name));

                var parametro = new ParametroGetParametro()
                {
                    CodigoEmpresa = pRequest.CodigoEmpresa,
                    CodigoSucursal = pRequest.CodigoSucursal,
                    CodigoItem = (int)EnumCodigoItemParametro.CodigoItemTriage,
                    CodigoGrupo = (int)EnumCodigoGrupoParametro.CodigoGrupoTriage,
                    VigenciaParametro = EnumVigencia.S.ToString()
                };

                codigoItem2 = _triageRepositorio.GetCodigoItemDosParametro(parametro);

                LoggingProvider.LogTraza(string.Format("Salida: {0}", codigoItem2), Prioridad.Traza, string.Format("Clase: {0}, Método: {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                return string.IsNullOrWhiteSpace(codigoItem2) ? "N" : codigoItem2;
            }
            catch (Exception ex)
            {
                if (ex.InnerException is ExcepcionNegocio || ex.InnerException is ExcepcionBaseDatos || ex.InnerException is ExcepcionAplicacion)
                {
                    throw ex.InnerException;
                }
                else
                {
                    throw new ExcepcionAplicacion(string.Format("Clase: {0}, Método: {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name), ex);
                }
            }
            finally
            {
                codigoItem2 = null;
            }
        }

        #endregion

        #region Dispose

        /// <summary>
        /// Provee un mecanismo de liberación de recursos no manejados.
        /// </summary>
        public void Dispose()
        {
            if (_triageRepositorio != null)
            {
                _triageRepositorio = null;
            }

            GC.SuppressFinalize(this);
        }

        #endregion
    }
}