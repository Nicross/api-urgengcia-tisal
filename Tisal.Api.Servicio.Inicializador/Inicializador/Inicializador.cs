﻿using AutoMapper;
using System.ComponentModel.Composition;
using Tisal.Api.Servicio.Core;
using Tisal.NCommon.ComponentModel;

namespace Tisal.Api.Servicio.Inicializador
{
    /// <summary>
    /// Inicializa una instancia del servicio en base a su contrato
    /// </summary>
    [Export(typeof(IModulo))]
    public class Inicializador : IModulo
    {
        /// <summary>
        /// Método para inicializar servicios.
        /// </summary>
        /// <param name="registrar">Interfaz de RegistraModulo.</param>
        public void Initialize(IRegistraModulo registrar)
        {
            registrar.Registrar<IMapper, Mapper>();
            registrar.Registrar<ITriageServicio, TriageServicio>();
        }
    }
}