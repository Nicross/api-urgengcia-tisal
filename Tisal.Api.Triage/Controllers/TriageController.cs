﻿using System;
using System.Web.Http;
using System.Web.Http.Description;
using Tisal.Api.Servicio.Core;
using Tisal.NCommon.Excepciones;

namespace Tisal.Api.Triage.Controllers
{
    /// <summary>
    /// Contiene los métodos relacionados con Triage.
    /// </summary>
    [RoutePrefix("triage/api/v1")]
    public class TriageController : ApiController
    {
        #region Miembros

        /// <summary>
        /// Contrato de servicio
        /// </summary>
        private readonly ITriageServicio _triageServicio;

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor predeterminado
        /// </summary>
        /// <param name="pTriageServicio">Contrato de servicio</param>
        public TriageController(ITriageServicio pTriageServicio)
        {
            _triageServicio = pTriageServicio;
        }

        #endregion

        #region Métodos Base

        /// <summary>
        /// Método que permite insertar un nuevo registro de triage
        /// </summary>
        /// <param name="pRequest">Objeto request con los parámetros necesarios para la solicitud</param>
        /// <returns>Objeto Meta-Response con la respuesta de la solicitud</returns>
        [Route("addtriage")]
        [HttpPost]
        [ResponseType(typeof(MetaResponseAddTriage))]
        public IHttpActionResult AddTriage([FromBody]AddTriageRequest pRequest)
        {
            var _metaAddResponse = new MetaResponseAddTriage();

            try
            {
                if (pRequest == null)
                {
                    _metaAddResponse.Header.ConstruirRespuesta("04", MensajesApi.ErrorRequest);
                }
                else
                {
                    var validaRequest = _triageServicio.ValidaAddTriage(pRequest);

                    if (!string.IsNullOrWhiteSpace(validaRequest))
                    {
                        _metaAddResponse.Header.ConstruirRespuesta("04", validaRequest);
                    }
                    else
                    {
                        _metaAddResponse.Body.Respuesta = _triageServicio.AddTriage(pRequest);
                        _metaAddResponse.Header.ConstruirRespuesta("00", MensajesApi.EjecucionExitosa);
                    }
                }
            }
            catch (ExcepcionBaseDatos ex)
            {
                _metaAddResponse.Header.ConstruirRespuesta("01", MensajesApi.ErrorDatos);
                ProviderException.ManejaExcepcion(ex, "DataPolitica");
            }
            catch (ExcepcionAplicacion ex)
            {
                _metaAddResponse.Header.ConstruirRespuesta("02", MensajesApi.ErrorAplicacion);
                ProviderException.ManejaExcepcion(ex, "NegocioPolitica");
            }
            catch (ExcepcionNegocio ex)
            {
                _metaAddResponse.Header.ConstruirRespuesta("02", MensajesApi.ErrorAplicacion);
                ProviderException.ManejaExcepcion(ex, "NegocioPolitica");
            }
            catch (Exception ex)
            {
                _metaAddResponse.Header.ConstruirRespuesta("03", MensajesApi.ErrorGenerico);
                ProviderException.ManejaExcepcion(ex, "GenericaPolitica");
            }

            return Ok(_metaAddResponse);
        }

        /// <summary>
        /// Método que permite obtener Triage.
        /// </summary>
        /// <param name="pRequest">Objeto request con los parámetros necesarios para la solicitud</param>
        /// <returns>Objeto Meta-Response con la respuesta de la solicitud</returns>
        [Route("gettriage")]
        [HttpPost]
        [ResponseType(typeof(MetaResponseGetTriage))]
        public IHttpActionResult GetTriage([FromBody]GetTriageRequest pRequest)
        {
            var _metaGetResponse = new MetaResponseGetTriage();

            try
            {
                if (pRequest == null)
                {
                    _metaGetResponse.Header.ConstruirRespuesta("04", MensajesApi.ErrorRequest);
                }
                else
                {
                    var validaRequest = _triageServicio.ValidaGetTriage(pRequest);

                    if (!string.IsNullOrWhiteSpace(validaRequest))
                    {
                        _metaGetResponse.Header.ConstruirRespuesta("04", validaRequest);
                    }
                    else
                    {
                        _metaGetResponse.Body.Respuesta = _triageServicio.GetTriage(pRequest);
                        _metaGetResponse.Header.ConstruirRespuesta("00", MensajesApi.EjecucionExitosa);
                    }
                }
            }
            catch (ExcepcionBaseDatos ex)
            {
                _metaGetResponse.Header.ConstruirRespuesta("01", MensajesApi.ErrorDatos);
                ProviderException.ManejaExcepcion(ex, "DataPolitica");
            }
            catch (ExcepcionAplicacion ex)
            {
                _metaGetResponse.Header.ConstruirRespuesta("02", MensajesApi.ErrorAplicacion);
                ProviderException.ManejaExcepcion(ex, "NegocioPolitica");
            }
            catch (ExcepcionNegocio ex)
            {
                _metaGetResponse.Header.ConstruirRespuesta("02", MensajesApi.ErrorAplicacion);
                ProviderException.ManejaExcepcion(ex, "NegocioPolitica");
            }
            catch (Exception ex)
            {
                _metaGetResponse.Header.ConstruirRespuesta("03", MensajesApi.ErrorGenerico);
                ProviderException.ManejaExcepcion(ex, "GenericaPolitica");
            }

            return Ok(_metaGetResponse);
        }

        #endregion
    }
}