﻿using System;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Dispatcher;
using Tisal.NCommon.Excepciones;

namespace Tisal.Api.Triage.Controllers
{
    /// <summary>
    /// Maneja solicitudes no controlables desde el controlador del API.
    /// </summary>
    public class ExceptionHandlingControllerActivator : IHttpControllerActivator
    {
        private readonly IHttpControllerActivator _concreteActivator;

        /// <summary>
        /// Maneja excepciones que impiden crear el controlador.
        /// </summary>
        /// <param name="concreteActivator"></param>
        public ExceptionHandlingControllerActivator(IHttpControllerActivator concreteActivator)
        {
            _concreteActivator = concreteActivator;
        }

        /// <summary>
        /// Sobrecarga la función que se encarga de crear los controladores.
        /// </summary>
        /// <param name="request">Request asociado al controlador.</param>
        /// <param name="controllerDescriptor">Descripción del controlador requerido.</param>
        /// <param name="controllerType">Tipo.</param>
        /// <returns>Http Response manejado.</returns>
        public IHttpController Create(HttpRequestMessage request, HttpControllerDescriptor controllerDescriptor, Type controllerType)
        {
            try
            {
                return _concreteActivator.Create(request, controllerDescriptor, controllerType);
            }
            catch (InvalidOperationException ex)
            {
                ProviderException.ManejaExcepcion(ex, "GenericaPolitica");
                return null;
            }
        }
    }
}