﻿using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using System;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.Web.Http.Dispatcher;
using System.Web.Mvc;
using Tisal.Api.Triage.Controllers;
using Tisal.NCommon.Logging;

namespace Tisal.Api.Triage
{
    /// <summary>
    /// Clase principal que inicializa el wep api
    /// </summary>
    public class Global : HttpApplication
    {
        /// <summary>
        /// Inicializa la aplicacion
        /// </summary>
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configuration.Services.Replace(typeof(IHttpControllerActivator), new ExceptionHandlingControllerActivator(GlobalConfiguration.Configuration.Services.GetHttpControllerActivator()));
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            UnityConfig.RegisterComponents();

            IConfigurationSource config = ConfigurationSourceFactory.Create();
            ExceptionPolicyFactory factory = new ExceptionPolicyFactory(config);
            Logger.SetLogWriter(new LogWriterFactory().Create());
            ExceptionManager exManager = factory.CreateManager();
            ExceptionPolicy.SetExceptionManager(factory.CreateManager());
        }

        /// <summary>
        /// Maneja errores internos, no controlados.
        /// </summary>
        /// <param name="sender">Objeto origen que ocasionó el error.</param>
        /// <param name="e">Argumento enviado.</param>
        protected void Application_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();

            if (!(ex is ThreadAbortException))
            {
                if (ex is HttpUnhandledException)
                {
                    ex = ((HttpUnhandledException)ex).InnerException;
                }

                LoggingProvider.LogError(ex, Prioridad.Alta, string.Empty);
            }
        }
    }
}
