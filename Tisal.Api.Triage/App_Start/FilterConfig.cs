﻿using System.Web.Mvc;

namespace Tisal.Api.Triage
{
    /// <summary>
    /// Clase que se encarga de cargar los filtros del api
    /// </summary>
    public class FilterConfig
    {
        /// <summary>
        /// Metodo estatico encargado de agregar los filtros al web api
        /// </summary>
        /// <param name="filters"></param>
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}