﻿using System.Configuration;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Tisal.Api.Triage
{
    /// <summary>
    /// Configuración de la WebApi
    /// </summary>
    public static class WebApiConfig
    {
        /// <summary>
        /// Método para registrar la configuración
        /// </summary>
        /// <param name="config"></param>
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));

            var origen = ConfigurationManager.AppSettings["origen"];
            var cors = new EnableCorsAttribute(origen, "*", "*");
            config.EnableCors(cors);

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
