using System.Web.Http;
using Tisal.NCommon.ComponentModel;
using Unity;

namespace Tisal.Api.Triage
{
    /// <summary>
    /// Clase est�.tica que carga las dependencias de Unity
    /// </summary>
    public static class UnityConfig
    {
        /// <summary>
        /// Registra configuraciones de inicio
        /// </summary>
        public static void RegisterComponents()
        {
            var container = new UnityContainer();
            CargadorModulo.CargarContenedor(container, ".\\bin", "*.Inicializador.dll");
            CargadorMapeos.CargarPerfiles(container, ".\\bin", "*.Servicio.dll");
            GlobalConfiguration.Configuration.DependencyResolver = new Unity.WebApi.UnityDependencyResolver(container);
        }
    }
}