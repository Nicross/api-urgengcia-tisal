using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Tisal.Api.Triage.Areas.HelpPage.ModelDescriptions
{
    /// <summary>
    /// Self-generated comment
    /// </summary>
    public class EnumTypeModelDescription : ModelDescription
    {
        /// <summary>
        /// Self-generated comment
        /// </summary>
        public EnumTypeModelDescription()
        {
            Values = new Collection<EnumValueDescription>();
        }

        /// <summary>
        /// Self-generated comment
        /// </summary>
        public Collection<EnumValueDescription> Values { get; private set; }
    }
}