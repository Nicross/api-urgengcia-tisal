using System;
using System.Reflection;

namespace Tisal.Api.Triage.Areas.HelpPage.ModelDescriptions
{
    /// <summary>
    /// Self-generated comment
    /// </summary>
    public interface IModelDocumentationProvider
    {
        /// <summary>
        /// Self-generated comment
        /// </summary>
        string GetDocumentation(MemberInfo member);

        /// <summary>
        /// Self-generated comment
        /// </summary>
        string GetDocumentation(Type type);
    }
}