using System;

namespace Tisal.Api.Triage.Areas.HelpPage.ModelDescriptions
{
    /// <summary>
    /// Describes a type model.
    /// </summary>
    public abstract class ModelDescription
    {
        /// <summary>
        /// Self-generated comment
        /// </summary>
        public string Documentation { get; set; }

        /// <summary>
        /// Self-generated comment
        /// </summary>
        public Type ModelType { get; set; }

        /// <summary>
        /// Self-generated comment
        /// </summary>
        public string Name { get; set; }
    }
}