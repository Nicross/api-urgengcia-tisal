using System;

namespace Tisal.Api.Triage.Areas.HelpPage.ModelDescriptions
{
    /// <summary>
    /// Self-generated comment
    /// </summary>
    public class ParameterAnnotation
    {
        /// <summary>
        /// Self-generated comment
        /// </summary>
        public Attribute AnnotationAttribute { get; set; }

        /// <summary>
        /// Self-generated comment
        /// </summary>
        public string Documentation { get; set; }
    }
}