using System.Collections.ObjectModel;

namespace Tisal.Api.Triage.Areas.HelpPage.ModelDescriptions
{
    /// <summary>
    /// Self-generated comment
    /// </summary>
    public class ComplexTypeModelDescription : ModelDescription
    {
        /// <summary>
        /// Self-generated comment
        /// </summary>
        public ComplexTypeModelDescription()
        {
            Properties = new Collection<ParameterDescription>();
        }

        /// <summary>
        /// Self-generated comment
        /// </summary>
        public Collection<ParameterDescription> Properties { get; private set; }
    }
}