namespace Tisal.Api.Triage.Areas.HelpPage.ModelDescriptions
{
    /// <summary>
    /// Self-generated comment
    /// </summary>
    public class KeyValuePairModelDescription : ModelDescription
    {
        /// <summary>
        /// Self-generated comment
        /// </summary>
        public ModelDescription KeyModelDescription { get; set; }

        /// <summary>
        /// Self-generated comment
        /// </summary>
        public ModelDescription ValueModelDescription { get; set; }
    }
}