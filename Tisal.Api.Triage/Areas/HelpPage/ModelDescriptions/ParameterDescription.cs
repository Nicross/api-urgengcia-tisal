using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Tisal.Api.Triage.Areas.HelpPage.ModelDescriptions
{
    /// <summary>
    /// Self-generated comment
    /// </summary>
    public class ParameterDescription
    {
        /// <summary>
        /// Self-generated comment
        /// </summary>
        public ParameterDescription()
        {
            Annotations = new Collection<ParameterAnnotation>();
        }

        /// <summary>
        /// Self-generated comment
        /// </summary>
        public Collection<ParameterAnnotation> Annotations { get; private set; }

        /// <summary>
        /// Self-generated comment
        /// </summary>
        public string Documentation { get; set; }

        /// <summary>
        /// Self-generated comment
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Self-generated comment
        /// </summary>
        public ModelDescription TypeDescription { get; set; }
    }
}