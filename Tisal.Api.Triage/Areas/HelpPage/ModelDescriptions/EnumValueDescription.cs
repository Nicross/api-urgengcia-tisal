namespace Tisal.Api.Triage.Areas.HelpPage.ModelDescriptions
{
    /// <summary>
    /// Self-generated comment
    /// </summary>
    public class EnumValueDescription
    {
        /// <summary>
        /// Self-generated comment
        /// </summary>
        public string Documentation { get; set; }

        /// <summary>
        /// Self-generated comment
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Self-generated comment
        /// </summary>
        public string Value { get; set; }
    }
}