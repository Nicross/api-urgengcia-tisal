namespace Tisal.Api.Triage.Areas.HelpPage
{
    /// <summary>
    /// Indicates whether the sample is used for request or response
    /// </summary>
    public enum SampleDirection
    {
        /// <summary>
        /// Self-generated comment
        /// </summary>
        Request = 0,
        /// <summary>
        /// Self-generated comment
        /// </summary>
        Response
    }
}