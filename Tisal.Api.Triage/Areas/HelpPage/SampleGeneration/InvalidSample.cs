using System;

namespace Tisal.Api.Triage.Areas.HelpPage
{
    /// <summary>
    /// This represents an invalid sample on the help page. There's a display template named InvalidSample associated with this class.
    /// </summary>
    public class InvalidSample
    {
        /// <summary>
        /// Self-generated comment
        /// </summary>
        public InvalidSample(string errorMessage)
        {
            if (errorMessage == null)
            {
                throw new ArgumentNullException("errorMessage");
            }
            ErrorMessage = errorMessage;
        }

        /// <summary>
        /// Self-generated comment
        /// </summary>
        public string ErrorMessage { get; private set; }

        /// <summary>
        /// Self-generated comment
        /// </summary>
        public override bool Equals(object obj)
        {
            InvalidSample other = obj as InvalidSample;
            return other != null && ErrorMessage == other.ErrorMessage;
        }

        /// <summary>
        /// Self-generated comment
        /// </summary>
        public override int GetHashCode()
        {
            return ErrorMessage.GetHashCode();
        }

        /// <summary>
        /// Self-generated comment
        /// </summary>
        public override string ToString()
        {
            return ErrorMessage;
        }
    }
}