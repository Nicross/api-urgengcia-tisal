using System;

namespace Tisal.Api.Triage.Areas.HelpPage
{
    /// <summary>
    /// This represents a preformatted text sample on the help page. There's a display template named TextSample associated with this class.
    /// </summary>
    public class TextSample
    {
        /// <summary>
        /// Self-generated comment
        /// </summary>
        public TextSample(string text)
        {
            if (text == null)
            {
                throw new ArgumentNullException("text");
            }
            Text = text;
        }

        /// <summary>
        /// Self-generated comment
        /// </summary>
        public string Text { get; private set; }

        /// <summary>
        /// Self-generated comment
        /// </summary>
        public override bool Equals(object obj)
        {
            TextSample other = obj as TextSample;
            return other != null && Text == other.Text;
        }

        /// <summary>
        /// Self-generated comment
        /// </summary>
        public override int GetHashCode()
        {
            return Text.GetHashCode();
        }

        /// <summary>
        /// Self-generated comment
        /// </summary>
        public override string ToString()
        {
            return Text;
        }
    }
}