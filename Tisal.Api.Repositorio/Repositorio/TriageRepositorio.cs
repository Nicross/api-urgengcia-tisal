﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Reflection;
using Tisal.Api.Repositorio.Core;
using Tisal.Api.Utilitario;
using Tisal.NCommon.Data;
using Tisal.NCommon.Excepciones;
using Tisal.NCommon.Logging;
using Tisal.NCommon.Utility;

namespace Tisal.Api.Repositorio
{
    /// <summary>
    /// Implementación del contrato de repositorio
    /// </summary>
    public class TriageRepositorio : DbRepository, ITriageRepositorio
    {
        #region Propiedades

        /// <summary>
        /// Propiedad con el Schema de la base de datos.
        /// </summary>
        private string Schema { get; set; }

        /// <summary>
        /// Nombre de la conexión configurada actualmente.
        /// </summary>
        private string NombreConexion { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor predeterminado.
        /// </summary>
        public TriageRepositorio()
            : base(Constantes.ConnRCEDB)
        {
            NombreConexion = Constantes.ConnRCEDB;
            Schema = this.DefaultSchema;
        }

        #endregion

        #region Métodos Base

        /// <summary>
        /// Método que controla la transacción al agregar Triage.
        /// </summary>
        /// <param name="pParametros">Objeto de parámetros con las propiedades necesarias para la solicitud.</param>
        /// <returns>Retorna éxito de operación.</returns>
        public bool AddTriageTrx(ParametroAddTriage pParametros)
        {
            bool triageRespuesta = false;

            try
            {
                using (DbConnection conn = Database.GetOpenDbConnection())
                {
                    using (DbTransaction transaction = conn.BeginTransaction(IsolationLevel.ReadCommitted))
                    {
                        try
                        {
                            var idEvaluacion = AddTriage(pParametros, transaction);

                            if (idEvaluacion > 0)
                            {
                                var respuestaUpdateTriage = UpdateTriageAreaTriage(pParametros, transaction);

                                if (respuestaUpdateTriage)
                                {
                                    if (pParametros.ListadoParametroClinico != null && pParametros.ListadoParametroClinico.Count > 0)
                                    {
                                        if (AddParametroClinico(pParametros, idEvaluacion, transaction))
                                        {
                                            var respuestaUpdateAdmision = pParametros.CodigoItemDos.Equals("S") ? UpdateTriageAreaAdmision(pParametros, transaction) : true;

                                            if (respuestaUpdateAdmision)
                                            {
                                                triageRespuesta = true;
                                                transaction.Commit();
                                            }
                                            else
                                            {
                                                transaction.Rollback();
                                            }
                                        }
                                        else
                                        {
                                            transaction.Rollback();
                                        }
                                    }
                                    else
                                    {
                                        var respuestaUpdateAdmision = pParametros.CodigoItemDos.Equals("S") ? UpdateTriageAreaAdmision(pParametros, transaction) : true;

                                        if (respuestaUpdateAdmision)
                                        {
                                            triageRespuesta = true;
                                            transaction.Commit();
                                        }
                                        else
                                        {
                                            transaction.Rollback();
                                        }
                                    }
                                }
                                else
                                {
                                    transaction.Rollback();
                                }
                            }
                            else
                            {
                                transaction.Rollback();
                            }
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            LoggingProvider.LogError(ex.InnerException, Prioridad.Alta, this.GetType().Name);
                        }
                        finally
                        {
                            if (conn.State == ConnectionState.Open)
                            {
                                conn.Close();
                            }
                        }
                    }
                }

                return triageRespuesta;
            }
            catch (OracleException ora)
            {
                throw new ExcepcionBaseDatos(string.Format("Clase: {0}, Método: {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name), ora);
            }
            catch (Exception ex)
            {
                throw new ExcepcionBaseDatos(string.Format("Clase: {0}, Método: {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name), ex);
            }
        }

        /// <summary>
        /// Método que obtiene Triage.
        /// </summary>
        /// <param name="pParametros">Objeto de parámetros con las propiedades necesarias para la solicitud.</param>
        /// <returns>Retorna listado de objetos triage.</returns>
        public IEnumerable<Triage> GetTriage(ParametroGetTriage pParametros)
        {
            List<Triage> listaTriage = null;

            try
            {
                var procedimiento = Procedimientos.prcGetTriage.Split('.');
                listaTriage = new List<Triage>();

                using (var cmd = Database.CreateStoredProcedureCommand(procedimiento[1], procedimiento[0], Schema))
                {
                    Database.AddParam(cmd, pParametros.CodigoEmpresa, "PNumCodEmpresa"); // NOT NULL
                    Database.AddParam(cmd, pParametros.CodigoSucursal, "PNumCodSucursal"); // NOT NULL
                    Database.AddParam(cmd, pParametros.CodigoOrigen, "PNumCodOrigen"); // NOT NULL

                    if (pParametros.IdAtencion == null || pParametros.IdAtencion == 0)
                        Database.AddParam(cmd, DBNull.Value, "PNumIdAtencion");
                    else
                        Database.AddParam(cmd, pParametros.IdAtencion, "PNumIdAtencion");

                    Database.AddParam(cmd, pParametros.IdAtencionLyra, "PNumIdAtencionLyra"); // NOT NULL
                    Database.AddParam(cmd, pParametros.VigenciaTriage, "PStrVigencia"); // NOT NULL
                    Database.AddParam(cmd, pParametros.VigenciaUsuario, "PStrVigenciaUsuario"); // NOT NULL                                                       

                    Database.AddSpecialParam(cmd, "SCursor");

                    using (var reader = Database.ExecuteReader(cmd))
                    {
                        if (reader != null)
                        {
                            while (reader.Read())
                            {
                                listaTriage.Add(new Triage()
                                {
                                    FechaInicioTriage = reader["FECHA_INICIO_TRIAGE"].MapperCastDateTime(),
                                    Evaluacion = reader["EVALUACION"].MapperCastInteger(),
                                    CodigoClasificacion = reader["COD_CLASIFICACION"].MapperCastString(),
                                    MotivoConsulta = reader["MOTIVO_DE_CONSULTA"].MapperCastString(),
                                    DescripcionAlergia = reader["DESCRIP_ALERGIA"].MapperCastString(),
                                    DescripcionAntecedente = reader["DESCRIP_ANTECEDENTE"].MapperCastString(),
                                    Abreviatura = reader["ABREVIATURA"].MapperCastString(),
                                    ApellidoPaterno = reader["AP_PATERNO"].MapperCastString(),
                                    ApellidoMaterno = reader["AP_MATERNO"].MapperCastString(),
                                    Nombre = reader["NOMBRE"].MapperCastString()
                                });
                            }
                        }
                    }

                    cmd.Connection.Close();
                }

                procedimiento = null;
                return listaTriage;
            }
            catch (OracleException ora)
            {
                throw new ExcepcionBaseDatos(string.Format("Clase: {0}, Método: {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name), ora);
            }
            catch (Exception ex)
            {
                throw new ExcepcionBaseDatos(string.Format("Clase: {0}, Método: {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name), ex);
            }
            finally
            {
                listaTriage = null;
            }
        }

        /// <summary>
        /// Método que obtiene el cédigo de item 2 del parámetro.
        /// </summary>
        /// <param name="pParametros">Objeto de parámetros con las propiedades necesarias para la solicitud.</param>
        /// <returns>Retorna strign con el código de item 2 del parámetro</returns>
        public string GetCodigoItemDosParametro(ParametroGetParametro pParametros)
        {
            string codigoItem2 = null;

            try
            {
                var procedimiento = Procedimientos.prcGetParametro.Split('.');

                using (var cmd = Database.CreateStoredProcedureCommand(procedimiento[1], procedimiento[0], Schema))
                {
                    Database.AddParam(cmd, pParametros.CodigoEmpresa, "PNumCodEmpresa");
                    Database.AddParam(cmd, pParametros.CodigoSucursal, "PNumCodSucursal");
                    Database.AddParam(cmd, pParametros.CodigoGrupo, "PNumCodGrupo");
                    Database.AddParam(cmd, pParametros.CodigoItem, "PNumCodItem");
                    Database.AddParam(cmd, pParametros.VigenciaParametro, "PVigenciaParametro");
                    Database.AddSpecialParam(cmd, "RCurSor");

                    using (var reader = Database.ExecuteReader(cmd))
                    {
                        if (reader != null)
                        {
                            while (reader.Read())
                            {
                                codigoItem2 = reader["USO_COD_ITEM2"].MapperCastString();
                            }
                        }
                    }

                    cmd.Connection.Close();
                }

                procedimiento = null;
                return codigoItem2;
            }
            catch (OracleException ora)
            {
                throw new ExcepcionBaseDatos(string.Format("Clase: {0}, Método: {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name), ora);
            }
            catch (Exception ex)
            {
                throw new ExcepcionBaseDatos(string.Format("Clase: {0}, Método: {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name), ex);
            }
            finally
            {
                codigoItem2 = null;
            }
        }

        #endregion

        #region Métodos Privados

        /// <summary>
        /// Método que inserta Triage.
        /// </summary>
        /// <param name="pParametros">Objeto de parámetros con las propiedades necesarias para la solicitud.</param>
        /// <param name="pTransaction">Transaction con base de datos.</param>
        /// <returns>Retorna éxito de operación.</returns>
        private int AddTriage(ParametroAddTriage pParametros, DbTransaction pTransaction)
        {
            try
            {
                var pIdEvaluacionRespuesta = 0;
                var procedimiento = Procedimientos.prcAddTriage.Split('.');

                using (var cmd = Database.CreateStoredProcedureCommand(procedimiento[1], procedimiento[0], Schema, pTransaction))
                {
                    Database.AddParam(cmd, pParametros.CodigoEmpresa, "pCodEmpresa"); // NOT NULL
                    Database.AddParam(cmd, pParametros.CodigoSucursal, "pCodSucursal"); // NOT NULL
                    Database.AddParam(cmd, pParametros.IdAtencion, "pIdAtencion"); // NOT NULL
                    Database.AddParam(cmd, pParametros.IdAtencionLyra, "pIdAtencionLyra"); // NOT NULL

                    if (pParametros.CodigoClasificacion == null || pParametros.CodigoClasificacion == 0)
                        Database.AddParam(cmd, DBNull.Value, "pCodClasificacion");
                    else
                        Database.AddParam(cmd, pParametros.CodigoClasificacion, "pCodClasificacion");

                    if (string.IsNullOrWhiteSpace(pParametros.MotivoConsulta))
                        Database.AddParam(cmd, DBNull.Value, "pMotivoConsulta");
                    else
                        Database.AddParam(cmd, pParametros.MotivoConsulta, "pMotivoConsulta");

                    if (string.IsNullOrWhiteSpace(pParametros.DescripcionAlergia))
                        Database.AddParam(cmd, DBNull.Value, "pDescripcionAlergia");
                    else
                        Database.AddParam(cmd, pParametros.DescripcionAlergia, "pDescripcionAlergia");

                    if (string.IsNullOrWhiteSpace(pParametros.DescripcionAntecendente))
                        Database.AddParam(cmd, DBNull.Value, "pDescripcionAntecedente");
                    else
                        Database.AddParam(cmd, pParametros.DescripcionAntecendente, "pDescripcionAntecedente");

                    if (!pParametros.FechaInicioTriage.ValidaFecha())
                        Database.AddParam(cmd, DBNull.Value, "pFechaInicioTriage");
                    else
                        Database.AddParam(cmd, pParametros.FechaInicioTriage.FormateaFecha(), "pFechaInicioTriage");

                    if (string.IsNullOrWhiteSpace(pParametros.IdUsuario))
                        Database.AddParam(cmd, DBNull.Value, "pIdUsuario");
                    else
                        Database.AddParam(cmd, pParametros.IdUsuario, "pIdUsuario");

                    if (string.IsNullOrWhiteSpace(pParametros.Vigencia))
                        Database.AddParam(cmd, DBNull.Value, "pVigencia");
                    else
                        Database.AddParam(cmd, pParametros.Vigencia, "pVigencia");

                    Database.AddParam(cmd, pParametros.NombreSecuencia, "pNombreSecuencia");
                    Database.AddParam(cmd, pParametros.AreaAtencion, "PNumAreaDeAtencion");
                    Database.AddParam(cmd, pIdEvaluacionRespuesta, "pIdEvaluacionRespuesta", ParameterDirection.Output);

                    cmd.ExecuteNonQuery();
                    pIdEvaluacionRespuesta = cmd.Parameters["pIdEvaluacionRespuesta"].Value.MapperCastInteger();
                }

                procedimiento = null;
                return pIdEvaluacionRespuesta;
            }
            catch (OracleException ora)
            {
                throw new ExcepcionBaseDatos(string.Format("Clase: {0}, Método: {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name), ora);
            }
            catch (Exception ex)
            {
                throw new ExcepcionBaseDatos(string.Format("Clase: {0}, Método: {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name), ex);
            }
        }

        /// <summary>
        /// Método que inserta parámetro clínico.
        /// </summary>
        /// <param name="pParametros">Objeto de parámetros con las propiedades necesarias para la solicitud.</param>
        /// <param name="pTransaction">Transaction con base de datos.</param>
        /// <returns>Retorna éxito de operación.</returns>
        private bool AddParametroClinico(ParametroAddTriage pParametros, int pIdEvaluacion, DbTransaction pTransaction)
        {
            try
            {
                bool respuesta = false;
                var procedimiento = Procedimientos.prcAddParametroClinico.Split('.');

                using (var cmd = Database.CreateStoredProcedureCommand(procedimiento[1], procedimiento[0], Schema, pTransaction))
                {
                    Database.AddParam(cmd, 0, "pCodEmpresa", ParameterDirection.Input); // NOT NULL
                    Database.AddParam(cmd, 0, "pCodSucursal", ParameterDirection.Input); // NOT NULL
                    Database.AddParam(cmd, 0, "pCodOrigen", ParameterDirection.Input); // NOT NULL
                    Database.AddParam(cmd, 0, "pIdAtencion", ParameterDirection.Input); // NOT NULL
                    Database.AddParam(cmd, 0, "pIdAtencionLyra", ParameterDirection.Input);
                    Database.AddParam(cmd, 0, "pEvaluacion", ParameterDirection.Input); // NOT NULL
                    Database.AddParam(cmd, 0, "pCodSeccion", ParameterDirection.Input); // NOT NULL
                    Database.AddParam(cmd, 0, "pCodItem", ParameterDirection.Input); // NOT NULL
                    Database.AddParam(cmd, string.Empty, "pValor", ParameterDirection.Input);
                    Database.AddParam(cmd, string.Empty, "pVigencia", ParameterDirection.Input);
                    Database.AddParam(cmd, string.Empty, "pIdUsuario", ParameterDirection.Input);
                    Database.AddParam(cmd, string.Empty, "pOrigenData", ParameterDirection.Input);

                    pParametros.ListadoParametroClinico.ForEach(item =>
                    {
                        cmd.Parameters["pCodEmpresa"].Value = pParametros.CodigoEmpresa; // NOT NULL
                        cmd.Parameters["pCodSucursal"].Value = pParametros.CodigoSucursal; // NOT NULL
                        cmd.Parameters["pCodOrigen"].Value = pParametros.CodigoOrigen; // NOT NULL
                        cmd.Parameters["pIdAtencion"].Value = pParametros.IdAtencion; // NOT NULL

                        if (pParametros.IdAtencionLyra == null || pParametros.IdAtencionLyra == 0)
                            cmd.Parameters["pIdAtencionLyra"].Value = DBNull.Value;
                        else
                            cmd.Parameters["pIdAtencionLyra"].Value = pParametros.IdAtencionLyra;

                        cmd.Parameters["pEvaluacion"].Value = pIdEvaluacion; // NOT NULL
                        cmd.Parameters["pCodSeccion"].Value = pParametros.CodigoSeccion; // NOT NULL
                        cmd.Parameters["pCodItem"].Value = item.CodigoItem; // NOT NULL

                        if (string.IsNullOrWhiteSpace(item.Valor))
                            cmd.Parameters["pValor"].Value = DBNull.Value;
                        else
                            cmd.Parameters["pValor"].Value = item.Valor;

                        if (string.IsNullOrWhiteSpace(item.Vigencia))
                            cmd.Parameters["pVigencia"].Value = DBNull.Value;
                        else
                            cmd.Parameters["pVigencia"].Value = item.Vigencia;

                        if (string.IsNullOrWhiteSpace(pParametros.IdUsuario))
                            cmd.Parameters["pIdUsuario"].Value = DBNull.Value;
                        else
                            cmd.Parameters["pIdUsuario"].Value = pParametros.IdUsuario;

                        if (string.IsNullOrWhiteSpace(pParametros.OrigenData))
                            cmd.Parameters["pOrigenData"].Value = DBNull.Value;
                        else
                            cmd.Parameters["pOrigenData"].Value = pParametros.OrigenData;

                        Database.ExecuteNonQuery(cmd);
                    });

                    respuesta = true;
                }

                procedimiento = null;
                return respuesta;
            }
            catch (OracleException ora)
            {
                throw new ExcepcionBaseDatos(string.Format("Clase: {0}, Método: {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name), ora);
            }
            catch (Exception ex)
            {
                throw new ExcepcionBaseDatos(string.Format("Clase: {0}, Método: {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name), ex);
            }
        }

        /// <summary>
        /// Método que actualiza una atención basado en la atención Triage.
        /// RCE_ATENCION
        /// </summary>
        /// <param name="pParametros">Objeto de parámetros con las propiedades necesarias para la solicitud.</param>
        /// <param name="pTransaction">Transaction con base de datos.</param>
        /// <returns>Retorna éxito de operación.</returns>
        private bool UpdateTriageAreaTriage(ParametroAddTriage pParametros, DbTransaction pTransaction)
        {
            try
            {
                bool respuesta = false;
                var procedimiento = Procedimientos.prcUpdateTriageAreaTriage.Split('.');

                using (var cmd = Database.CreateStoredProcedureCommand(procedimiento[1], procedimiento[0], Schema, pTransaction))
                {
                    Database.AddParam(cmd, pParametros.CodigoClasificacion, "PNumCodClasificacion"); // NOT NULL
                    Database.AddParam(cmd, pParametros.AreaAtencion, "PNumCodAreaAtencion"); // NOT NULL
                    Database.AddParam(cmd, pParametros.CodigoEmpresa, "PNumCodEmpresa"); // NOT NULL
                    Database.AddParam(cmd, pParametros.CodigoSucursal, "PNumCodSucursal"); // NOT NULL
                    Database.AddParam(cmd, pParametros.CodigoOrigen, "PNumCodOrigen"); // NOT NULL
                    Database.AddParam(cmd, pParametros.IdAtencion, "pIdAtencion"); // NOT NULL
                    Database.AddParam(cmd, pParametros.IdAtencionLyra, "pIdAtencionLyra"); // NOT NULL      

                    var resultado = Database.ExecuteNonQuery(cmd);
                    respuesta = resultado != 0;
                }

                procedimiento = null;
                return respuesta;
            }
            catch (OracleException ora)
            {
                throw new ExcepcionBaseDatos(string.Format("Clase: {0}, Método: {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name), ora);
            }
            catch (Exception ex)
            {
                throw new ExcepcionBaseDatos(string.Format("Clase: {0}, Método: {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name), ex);
            }
        }

        /// <summary>
        /// Método que actualiza una atención basado en la atención Triage
        /// URG_ADMISION
        /// </summary>
        /// <param name="pParametros">Objeto de parámetros con las propiedades necesarias para la solicitud.</param>
        /// <param name="pTransaction">Transaction con base de datos.</param>
        /// <returns>Retorna éxito de operación.</returns>
        private bool UpdateTriageAreaAdmision(ParametroAddTriage pParametros, DbTransaction pTransaction)
        {
            try
            {
                bool respuesta = false;
                var procedimiento = Procedimientos.prcUpdateTriageAreaExterno.Split('.');

                using (var cmd = Database.CreateStoredProcedureCommand(procedimiento[1], procedimiento[0], Schema, pTransaction))
                {
                    Database.AddParam(cmd, pParametros.CodigoClasificacion, "PNumCodClasificacion"); // NOT NULL
                    Database.AddParam(cmd, pParametros.AreaAtencion, "PNumCodAreaAtencion"); // NOT NULL
                    Database.AddParam(cmd, pParametros.IdUsuario, "PStrIdUsuario"); // NOT NULL
                    Database.AddParam(cmd, pParametros.CodigoEmpresa, "PNumCodEmpresa"); // NOT NULL
                    Database.AddParam(cmd, pParametros.CodigoSucursal, "PNumCodSucursal"); // NOT NULL
                    Database.AddParam(cmd, pParametros.IdAtencion, "PNumIdAtencion"); // NOT NULL                    

                    var resultado = Database.ExecuteNonQuery(cmd);
                    respuesta = resultado != 0;
                }

                procedimiento = null;
                return respuesta;
            }
            catch (OracleException ora)
            {
                throw new ExcepcionBaseDatos(string.Format("Clase: {0}, Método: {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name), ora);
            }
            catch (Exception ex)
            {
                throw new ExcepcionBaseDatos(string.Format("Clase: {0}, Método: {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name), ex);
            }
        }

        #endregion
    }
}