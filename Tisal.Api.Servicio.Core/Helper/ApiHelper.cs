﻿using System;
using Tisal.Api.Utilitario;

namespace Tisal.Api.Servicio.Core
{
    /// <summary>
    /// Clase estatica de ayuda para realizar las tareas del api repetitivas
    /// </summary>
    public static class ApiHelper
    {
        /// <summary>
        /// Genera el Header a partir de los parametros enviados
        /// </summary>
        /// <param name="headerReponse">Clase que contiene los parametros para la cabezera del reponse</param>
        /// <param name="pCode">codigo de respuesta</param>
        /// <param name="pMessage">mensajes generico para la respuesta</param>
        public static void ConstruirRespuesta(this HeaderResponse headerReponse, string pCode, string pMessage)
        {
            headerReponse.Code = pCode;
            headerReponse.Message = pMessage;
        }

        /// <summary>
        /// Genera la informacion adicional asociada a la excepcion lanzada
        /// </summary>
        /// <param name="cError">Excepcion controlada en el error</param>
        /// <returns>objeto con formato json</returns>
        public static object ObtieneObjetoJson(this Exception cError)
        {
            object retorno = null;

            try
            {
#if DEBUG
                retorno = cError.GetBaseException().ToJSONWithSetting().ReadFromJsonWithSetting<object>();
#endif
            }
            catch
            {
                retorno = null;
            }

            return retorno;
        }

        /// <summary>
        /// Genera la informacion adicional asociada a la excepcion lanzada
        /// </summary>
        /// <param name="cError">Excepcion controlada en el error</param>
        /// <returns>objeto con formato json</returns>
        public static Exception OriginalException(this Exception cError)
        {
            try
            {
                return cError != null ? cError.GetBaseException() : cError;
            }
            catch
            {
                try
                {
                    return cError.InnerException;
                }
                catch
                {
                    return cError;
                }
            }
        }
    }
}