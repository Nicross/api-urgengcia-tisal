﻿namespace Tisal.Api.Servicio.Core
{
    /// <summary>
    /// Base del request para el Api
    /// </summary>
    public class BaseRequest
    {
        /// <summary>
        /// Parametro opcional. Schema de Lyra.
        /// </summary>
        public string Schema { get; set; }
    }
}
