﻿using System.ComponentModel.DataAnnotations;

namespace Tisal.Api.Servicio.Core
{
    /// <summary>
    /// Contiene los parámetros requeridos por el request.
    /// </summary>
    public class GetTriageRequest
    {
        /// <summary>
        /// Obtiene o asigna Código de empresa.
        /// </summary>
        [Required]
        public int? CodigoEmpresa { get; set; }

        /// <summary>
        /// Obtiene o asigna Código de sucursal.
        /// </summary>
        [Required]
        public int? CodigoSucursal { get; set; }

        /// <summary>
        /// Obtiene o asigna Código de origen.
        /// </summary>
        [Required]
        public int? CodigoOrigen { get; set; }

        /// <summary>
        /// Obtiene o asigna Id de atención.
        /// </summary>
        [Required]
        public int? IdAtencion { get; set; }

        /// <summary>
        /// Obtiene o asigna Id de atención Lyra.
        /// </summary>
        [Required]
        public int? IdAtencionLyra { get; set; }

        /// <summary>
        /// Obtiene o asigna la vigencia del registro de triage, Ref: S
        /// RCE_DATOS_TRIAGE
        /// </summary>
        [Required]
        public string VigenciaTriage { get; set; }

        /// <summary>
        /// Obtiene o asigna la vigencia del usuario, Ref: S,
        /// RCE_USUARIOS
        /// </summary>
        [Required]
        public string VigenciaUsuario { get; set; }
    }
}
