﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Tisal.Api.Utilitario;

namespace Tisal.Api.Servicio.Core
{
    /// <summary>
    /// Contiene los parámetros requeridos por el request.
    /// </summary>
    public class AddTriageRequest
    {
        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public AddTriageRequest()
        {
            ListadoParametroClinico = new List<ParametroClinicoRequest>();
        }

        /// <summary>
        /// Campo obligatorio, 
        /// Obtiene o asigna Código de empresa.
        /// </summary>
        [Required]
        public int? CodigoEmpresa { get; set; }

        /// <summary>
        /// Campo obligatorio, 
        /// Obtiene o asigna Código de sucursal.
        /// </summary>
        [Required]
        public int? CodigoSucursal { get; set; }

        /// <summary>
        /// Campo obligatorio, 
        /// Obtiene o asigna Código de origen.
        /// </summary>
        [Required]
        public int? CodigoOrigen { get; set; }

        /// <summary>
        /// Campo obligatorio, 
        /// Obtiene o asigna Código de sección.
        /// </summary>
        [Required]
        public int? CodigoSeccion { get; set; }

        /// <summary>
        /// Campo obligatorio, 
        /// Obtiene o asigna Origen Data, Ref: T
        /// </summary>
        [Required]
        public string OrigenData { get; set; }

        /// <summary>
        /// Campo obligatorio, 
        /// Obtiene o asigna Id de atención.
        /// </summary>
        [Required]
        public int IdAtencion { get; set; }

        /// <summary>
        /// Campo obligatorio, 
        /// Obtiene o asigna Id atención Lyra.
        /// </summary>
        [Required]
        public int? IdAtencionLyra { get; set; }

        /// <summary>
        /// Campo obligatorio, 
        /// Obtiene o asigna Id usuario.
        /// </summary>
        [Required]
        public string IdUsuario { get; set; }

        /// <summary>
        /// Campo obligatorio, 
        /// Obtiene o asigna Código de clasificación.
        /// </summary>
        [Required]
        public int? CodigoClasificacion { get; set; }

        /// <summary>
        /// Campo obligatorio, 
        /// Obtiene o asigna Motivo de consulta.
        /// </summary>
        public string MotivoConsulta { get; set; }

        /// <summary>
        /// Obtiene o asigna Descripción de alergia.
        /// </summary>
        public string DescripcionAlergia { get; set; }

        /// <summary>
        /// Obtiene o asigna Descripción de antecedente.
        /// </summary>
        public string DescripcionAntecendente { get; set; }

        /// <summary>
        /// Campo obligatorio, 
        /// Obtiene o asigna Fehca inicio Triage
        /// Formato dd/MM/yyyy HH:mm:ss
        /// </summary>
        [Required]
        [JsonConverter(typeof(DateFormatConverter), "dd/MM/yyyy HH:mm:ss")]
        public DateTime FechaInicioTriage { get; set; }

        /// <summary>
        /// Atributo de la clase que identifica la vigencia
        /// </summary>
        private string vigencia;

        /// <summary>
        /// Campo obligatorio, 
        /// Vigencia del registro
        /// </summary>
        [Required]
        public string Vigencia
        {
            get
            {
                return string.IsNullOrWhiteSpace(vigencia) ? EnumVigencia.S.ToString() : vigencia;
            }
            set
            {
                vigencia = value;
            }
        }

        /// <summary>
        /// Campo obligatorio,
        /// Área de atención confirmada por el usuario en formulario triage.
        /// </summary>
        [Required]
        public string AreaAtencion { get; set; }

        /// <summary>
        /// Parámetros clínicos asociados al triage
        /// </summary>
        public List<ParametroClinicoRequest> ListadoParametroClinico { get; set; }

        /// <summary>
        /// Corresponde al nombre de la tabla relacionada a la secuencia.
        /// </summary>
        [JsonIgnore]
        [IgnoreDataMember]
        public string NombreSecuencia { get; set; }

        /// <summary>
        /// Código de item utilizado para verificar si procede o no efectuar un update al agregar un triage
        /// </summary>
        [JsonIgnore]
        [IgnoreDataMember]
        public string CodigoItemDos { get; set; }
    }
}