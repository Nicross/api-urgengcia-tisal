﻿namespace Tisal.Api.Servicio.Core
{
    /// <summary>
    /// Clase que representa las propiedades necesarias para la solicitud
    /// </summary>
    public class ParametroClinicoRequest
    {
        /// <summary>
        /// Código del parámetro clínico
        /// </summary>
        public int CodigoItem { get; set; }

        /// <summary>
        /// Valor del parámetro clínico.
        /// </summary>
        public string Valor { get; set; }

        /// <summary>
        /// Atributo de la clase que identifica la vigencia
        /// </summary>
        private string vigencia;

        /// <summary>
        /// Vigencia del registro
        /// </summary>
        public string Vigencia
        {
            get
            {
                return string.IsNullOrWhiteSpace(vigencia) ? "S" : vigencia;
            }
            set
            {
                vigencia = value;
            }
        }
    }
}