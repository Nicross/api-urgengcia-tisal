﻿using System;
using System.Collections.Generic;

namespace Tisal.Api.Servicio.Core
{
    /// <summary>
    /// Definición del contrato de servicio
    /// </summary>
    public interface ITriageServicio : IDisposable
    {
        /// <summary>
        /// Método que agrega Triage.
        /// </summary>
        /// <param name="pRequest">Objeto request con los parámetros necesarios para la solicitud</param>
        /// <returns>Retorna éxito de la operación.</returns>
        bool AddTriage(AddTriageRequest pRequest);

        /// <summary>
        /// Método que obtiene Triage.
        /// </summary>
        /// <param name="pRequest">Objeto request con los parámetros necesarios para la solicitud</param>
        /// <returns>Retorna listado de objetos triage.</returns>
        IEnumerable<TriageDTO> GetTriage(GetTriageRequest pRequest);

        /// <summary>
        /// Método que valida los parámetros de entrada del request
        /// </summary>
        /// <param name="pRequest">Objeto request con los parámetros necesarios para la solicitud</param>
        /// <returns>Retorna string con los errores detectados</returns>
        string ValidaAddTriage(AddTriageRequest pRequest);

        /// <summary>
        /// Método que valida los parámetros de entrada del request
        /// </summary>
        /// <param name="pRequest">Objeto request con los parámetros necesarios para la solicitud</param>
        /// <returns>Retorna string con los errores detectados</returns>
        string ValidaGetTriage(GetTriageRequest pRequest);
    }
}