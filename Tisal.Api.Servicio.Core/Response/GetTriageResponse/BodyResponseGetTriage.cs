﻿using System.Collections.Generic;

namespace Tisal.Api.Servicio.Core
{
    /// <summary>
    /// Contiene el cuerpo de la respuesta de GetTriage.
    /// </summary>
    public class BodyResponseGetTriage
    {
        /// <summary>
        /// Constructor predeterminado que inicializa la respuesta.
        /// </summary>
        public BodyResponseGetTriage()
        {
            Respuesta = null;
        }

        /// <summary>
        /// Contiene la respuesta esperada de Triage.
        /// </summary>
        public IEnumerable<TriageDTO> Respuesta { get; set; }
    }
}
