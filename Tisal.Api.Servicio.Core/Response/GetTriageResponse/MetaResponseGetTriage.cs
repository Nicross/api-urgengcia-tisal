﻿
namespace Tisal.Api.Servicio.Core
{
    /// <summary>
    /// Contiene la meta respuesta de GetTriage.
    /// </summary>
    public class MetaResponseGetTriage
    {
        /// <summary>
        /// Construcotr predeterminado que inicializa la meta respuesta.
        /// </summary>
        public MetaResponseGetTriage()
        {
            Header = new HeaderResponse();
            Body = new BodyResponseGetTriage();
        }

        /// <summary>
        /// Contiene la cabecera de la respuesta de GetTriage.
        /// </summary>
        public HeaderResponse Header { get; set; }

        /// <summary>
        /// Contiene el cuerpo de la respuesta de GetTriage.
        /// </summary>
        public BodyResponseGetTriage Body { get; set; }
    }
}
