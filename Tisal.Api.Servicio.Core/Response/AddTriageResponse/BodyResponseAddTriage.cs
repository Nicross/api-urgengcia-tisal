﻿
namespace Tisal.Api.Servicio.Core
{
    /// <summary>
    /// Contiene el cuerpo de la respuesta de AddTriage.
    /// </summary>
    public class BodyResponseAddTriage
    {
        /// <summary>
        /// Constructor predeterminado que inicializa la respuesta.
        /// </summary>
        public BodyResponseAddTriage()
        {
            Respuesta = false;
        }

        /// <summary>
        /// Contiene el indicador de éxito de AddTriage.
        /// </summary>
        public bool Respuesta { get; set; }
    }
}
