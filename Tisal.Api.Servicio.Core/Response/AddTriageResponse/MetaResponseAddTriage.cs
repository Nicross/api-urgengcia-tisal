﻿
namespace Tisal.Api.Servicio.Core
{
    /// <summary>
    /// Contiene la meta respuesta de AddTriage.
    /// </summary>
    public class MetaResponseAddTriage
    {
        /// <summary>
        /// Constructor predeterminado que incializa la meta respuesta.
        /// </summary>
        public MetaResponseAddTriage()
        {
            Header = new HeaderResponse();
            Body = new BodyResponseAddTriage();
        }

        /// <summary>
        /// Contiene la cabecera de la respuesta de AddTriage.
        /// </summary>
        public HeaderResponse Header { get; set; }

        /// <summary>
        /// Contiene el cuerpo de la respuesta de AddTriage.
        /// </summary>
        public BodyResponseAddTriage Body { get; set; }
    }
}
