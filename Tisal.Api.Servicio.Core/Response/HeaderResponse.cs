﻿namespace Tisal.Api.Servicio.Core
{
    /// <summary>
    /// Almacena datos de retorno para métodos de API
    /// </summary>
    public class HeaderResponse
    {
        /// <summary>
        /// Indica el código de retorno de la operación consumida
        /// <remarks>
        /// (00 para éxito)
        /// </remarks>
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Despliega mensaje de retorno a partir de operación realizada
        /// </summary>
        public string Message { get; set; }
    }
}
