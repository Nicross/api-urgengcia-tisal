﻿namespace Tisal.Api.Utilitario
{
    /// <summary>
    /// Enumerador que define las
    /// variables que serán validados para el request
    /// </summary>
    public enum EnumCamposValidacionRequest
    {
        /// <summary>
        /// Codigo de la empresa
        /// </summary>
        CODIGO_EMPRESA,
        /// <summary>
        /// Código de la sucursal
        /// </summary>
        CODIGO_SUCURSAL,
        /// <summary>
        /// Código de origen
        /// </summary>
        CODIGO_ORIGEN,
        /// <summary>
        /// Identificador de atención de Lyra
        /// </summary>
        ID_ATENCION_LYRA,
        /// <summary>
        /// Código de la sección
        /// </summary>
        CODIGO_SECCION,
        /// <summary>
        /// Origen de la información
        /// </summary>
        ORIGEN_DATA,
        /// <summary>
        /// Secuencia de oracle 
        /// </summary>
        SECUENCIA_ORACLE,
        /// <summary>
        /// Clasificación del triage
        /// </summary>
        CLASIFICACION_TRIAGE,
        /// <summary>
        /// Fecha de comienzo de la evaluación
        /// </summary>
        FECHA_INICIO_TRIAGE,
        /// <summary>
        /// Identificador del usuario
        /// </summary>
        ID_USUARIO,
        /// <summary>
        /// Vigencia de la atención
        /// </summary>
        VIGENCIA,
        /// <summary>
        /// Area de la atención
        /// </summary>
        AREA_ATENCION
    }

    /// <summary>
    /// Enumerador correspondiente a la vigencia de un registro
    /// </summary>
    public enum EnumVigencia
    {
        N = 0,
        S = 1
    }

    /// <summary>
    /// Enumerador correspondiente al prefijo de secuencias.
    /// </summary>
    public enum EnumNombreSecuencia
    {
        /// <summary>
        /// Corresponde a prefijo de secuencia para tabla RCE_PACIENTE.
        /// </summary>
        RCE_PACIENTE,
        /// <summary>
        /// Corresponde a prefijo de secuencia para tabla RCE_LOG_PACIENTE.
        /// </summary>
        RCE_LOG_PACIENTE,
        /// <summary>
        /// Corresponde a prefijo de secuencia para tabla RCE_EVENTO.
        /// </summary>
        RCE_EVENTO,
        /// <summary>
        /// Corresponde a prefijo de secuencia para tabla RCE_ATENCION.
        /// </summary>
        RCE_ATENCION,
        /// <summary>
        /// Corresponde a prefijo de secuencia para tabla RCE_TRASLADO.
        /// </summary>
        RCE_TRASLADO,
        /// <summary>
        /// Corresponde a prefijo de secuencia para tabla RCE_DATOS_TRIAGE.
        /// </summary>
        SEC_RCE_EVAL_EXAMFISICO
    }

    /// <summary>
    /// Enumerador que representa si se puede
    /// GRABAR INFORMACION EN TABLAS EXTERNAS (HIS)
    /// </summary>
    public enum EnumCodigoItemParametro
    {
        CodigoItemTriage = 231
    }

    /// <summary>
    /// Enumerador que representa el codigo de grupo para obtener si se puede
    /// GRABAR INFORMACION EN TABLAS EXTERNAS (HIS)
    /// </summary>
    public enum EnumCodigoGrupoParametro
    {
        CodigoGrupoTriage = 165
    }

    /// <summary>
    /// Enumerador correspondiente a GetTriage.
    /// </summary>
    public enum EnumGetTriage
    {
        /// <summary>
        /// Corresponde a Código de empresa.
        /// </summary>
        COD_EMPRESA,
        /// <summary>
        /// Correspode a Código de sucursal.
        /// </summary>
        COD_SUCURSAL,
        /// <summary>
        /// Corresponde a Código de origen.
        /// </summary>
        COD_ORIGEN,
        /// <summary>
        /// Corresponde a Id de atención.
        /// </summary>
        ID_ATENCION,
        /// <summary>
        /// Corresponde a Id de atención Lyra.
        /// </summary>
        ID_ATENCION_LYRA,
        /// <summary>
        /// Corresponde a Vigencia de triage.
        /// </summary>
        VIGENCIA_TRIAGE,
        /// <summary>
        /// Corresponde a Vigencia de usuario.
        /// </summary>
        VIGENCIA_USUARIO
    }
}