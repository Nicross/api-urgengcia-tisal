﻿using Newtonsoft.Json.Converters;

namespace Tisal.Api.Utilitario
{
    /// <summary>
    /// Clase para formatear la fecha de propiedades JSon
    /// </summary>
    public class DateFormatConverter : IsoDateTimeConverter
    {
        /// <summary>
        /// Formato para fecha Json
        /// </summary>
        /// <param name="format">Formato requerido</param>
        public DateFormatConverter(string format)
        {
            DateTimeFormat = format;
        }
    }
}