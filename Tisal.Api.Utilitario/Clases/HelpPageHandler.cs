﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tisal.Api.Utilitario
{
    /// <summary>
    /// Clase que contiene métodos de ayuda para realizar tareas repetitivas para el Web Api help page.
    /// </summary>
    public static class HelpPageHandler
    {
        /// <summary>
        /// Obtiene ejemplos según los tipos enviados.
        /// </summary>
        /// <param name="types"></param>
        public static List<string> ObtenerEjemploDatosSegunTipo(Type[] types)
        {
            List<string> propExample = new List<string>();

            foreach (Type t in types)
            {
                foreach (var p in t.GetProperties())
                {
                    string value = string.Empty;
                    TypeValueGeneratorExample.TryGetValue(p.PropertyType, out value);
                    propExample.Add(p.Name + "%3D" + value);
                }
            }

            return propExample;
        }

        /// <summary>
        /// Propiedad que obtiene un ejemplo de dato dependiendo del tipo enviado.
        /// </summary>
        private static readonly IDictionary<Type, string> TypeValueGeneratorExample = new Dictionary<Type, string>
        {
            { typeof(Int16), "-1" },
            { typeof(Int32), "-2" },
            { typeof(Int64), "-3" },
            { typeof(UInt16), "1" },
            { typeof(UInt32), "2" },
            { typeof(UInt64), "3" },
            { typeof(Byte), "0x00C9" },
            { typeof(Char), @"'c'" },
            { typeof(SByte), "-102" },
            { typeof(Single), "0.1" },
            { typeof(Double), "0.12" },
            { typeof(Decimal), "0.123" },
            { typeof(String), "ValorString" },
            { typeof(Guid), Guid.NewGuid().ToString() },
            { typeof(TimeSpan), TimeSpan.FromTicks(DateTime.Now.Ticks).ToString() },
            { typeof(DateTime), DateTime.Now.Date.ToString() },
            { typeof(DateTimeOffset), DateTime.UtcNow.ToShortDateString() },
            { typeof(Boolean), "true" },
        };
    }
}
