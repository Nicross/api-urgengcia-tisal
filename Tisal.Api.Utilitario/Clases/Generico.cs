﻿using System;
using System.Globalization;

namespace Tisal.Api.Utilitario
{
    /// <summary>
    /// Clase con tareas genéricas.
    /// </summary>
    public static class Generico
    {
        /// <summary>
        /// Obtiene un string con los nombres y valores de una clase enviada.
        /// </summary>
        /// <typeparam name="T">Tipo de clase.</typeparam>
        /// <param name="obj">Instancia de la clase de la cual se obtendrá sus propiedades.</param>
        /// <param name="separador">Se utiliza para delimitar entre propiedades.</param>
        /// <returns>String concatenado de las propieades de la clase enviada y sus valores.</returns>
        public static string ObtenerPropiedadesyValoresDesdeClase<T>(T obj, string separador = " | ")
        {
            string retorno = string.Empty, concat = " : ";

            try
            {
                foreach (var prop in obj.GetType().GetProperties())
                {
                    retorno = string.Concat(retorno, Convert.ToString(prop.Name), concat, (prop.GetValue(obj) != null ? prop.GetValue(obj) : string.Empty), separador);
                }

                return retorno.Trim();
            }
            catch
            {
                return string.Format("Catch: {0}", obj.GetType().ToString());
            }
        }

        /// <summary>
        /// Método que valida que la fecha sea fecha. 
        /// </summary>
        /// <param name="pFecha">Corresponde que se requiere validar</param>
        /// <param name="pFormato">Formato para la fecha, por defecto dd/MM/yyyy HH:mm:ss</param>
        /// <returns>Retorna bool con el resultado de la validación</returns>
        public static bool ValidaFecha(this DateTime pFecha, string pFormato = "")
        {
            try
            {
                var formatoFecha = string.IsNullOrWhiteSpace(pFormato) ? "dd/MM/yyyy HH:mm:ss" : pFormato;
                IFormatProvider cultura = new CultureInfo("es-CL", true);
                string fecha = DateTime.ParseExact(Convert.ToDateTime(pFecha).ToString(formatoFecha), formatoFecha, cultura).ToString(formatoFecha);
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Método que formatea fecha, Ref: dd/MM/yyyy HH:mm:ss
        /// </summary>
        /// <param name="pFecha">Corresponde que se requiere formatear</param>
        /// <param name="pFormato">Formato para la fecha, por defecto dd/MM/yyyy HH:mm:ss</param>
        /// <returns>Retorna bool con el resultado de la validación</returns>
        public static string FormateaFecha(this DateTime pFecha, string pFormato = "")
        {
            try
            {
                var formatoFecha = string.IsNullOrWhiteSpace(pFormato) ? "dd/MM/yyyy HH:mm:ss" : pFormato;
                IFormatProvider cultura = new CultureInfo("es-CL", true);
                var fechaRespuesta = DateTime.ParseExact(Convert.ToDateTime(pFecha).ToString(formatoFecha), formatoFecha, cultura).ToString(formatoFecha).Replace("-", "/");
                return fechaRespuesta;
            }
            catch
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Método que valida que la fecha sea fecha. 
        /// </summary>
        /// <param name="pFecha">Corresponde que se requiere validar</param>
        /// <param name="pFormato">Formato para la fecha, por defecto dd/MM/yyyy HH:mm:ss</param>
        /// <returns>Retorna bool con el resultado de la validación</returns>
        public static bool ValidaFecha(this DateTime? pFecha, string pFormato = "")
        {
            try
            {
                if (pFecha != null)
                {
                    var formatoFecha = string.IsNullOrWhiteSpace(pFormato) ? "dd/MM/yyyy HH:mm:ss" : pFormato;
                    IFormatProvider cultura = new CultureInfo("es-CL", true);
                    string fecha = DateTime.ParseExact(Convert.ToDateTime(pFecha).ToString(formatoFecha), formatoFecha, cultura).ToString(formatoFecha);
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Método que formatea fecha, Ref: dd/MM/yyyy HH:mm:ss
        /// </summary>
        /// <param name="pFecha">Corresponde que se requiere formatear</param>
        /// <param name="pFormato">Formato para la fecha, por defecto dd/MM/yyyy HH:mm:ss</param>
        /// <returns>Retorna bool con el resultado de la validación</returns>
        public static string FormateaFecha(this DateTime? pFecha, string pFormato = "")
        {
            try
            {
                var formatoFecha = string.IsNullOrWhiteSpace(pFormato) ? "dd/MM/yyyy HH:mm:ss" : pFormato;
                IFormatProvider cultura = new CultureInfo("es-CL", true);
                var fechaRespuesta = DateTime.ParseExact(Convert.ToDateTime(pFecha).ToString(formatoFecha), formatoFecha, cultura).ToString(formatoFecha).Replace("-", "/");
                return fechaRespuesta;
            }
            catch
            {
                return string.Empty;
            }
        }
    }
}
