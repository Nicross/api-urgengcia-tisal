﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tisal.Api.Utilitario
{
    /// <summary>
    /// Clase que provee ayuda para el tratamiento de objectos Json.
    /// </summary>
    public static class JsonHelper
    {
        #region Convertir en string Json

        /// <summary>
        /// Convierte un objeto en string en formato Json.
        /// </summary>
        /// <param name="obj">Objeto CLR válido.</param>
        /// <returns>String Json.</returns>
        public static string ToJSON(this object obj)
        {
            return JsonConvert.SerializeObject(obj);
        }

        /// <summary>
        /// Convierte un objeto en string en formato json, 
        /// configurado para pasar de objetos null y tomar listas predeterminadas
        /// </summary>
        /// <param name="obj">Objeto CLR válido.</param>
        /// <returns>String Json.</returns>
        public static string ToJSONWithSetting(this object obj)
        {
            var setting = new JsonSerializerSettings();
            setting.Converters.Add(new Newtonsoft.Json.Converters.JavaScriptDateTimeConverter());
            setting.NullValueHandling = NullValueHandling.Ignore;
            setting.TypeNameHandling = TypeNameHandling.Auto;
            setting.Formatting = Formatting.Indented;

            return JsonConvert.SerializeObject(obj, setting);
        }

        /// <summary>
        /// Convierte un objeto en string en formato json con caracteres extendidos (NO ASCII)
        /// </summary>
        /// <param name="obj">Objeto con formato válido para convertir en Jsons.</param>
        /// <returns>String con formato Json.</returns>
        public static string ToNonAsciiJSON(this object obj)
        {
            var sr = new StringWriter();

            var jsonWriter = new JsonTextWriter(sr)
            {
                StringEscapeHandling = StringEscapeHandling.EscapeNonAscii
            };

            new JsonSerializer().Serialize(jsonWriter, obj);

            return sr.ToString();
        }

        #endregion

        #region Convierte a Objeto

        /// <summary>
        /// Retorna un objeto del tipo T. 
        /// </summary>
        /// <typeparam name="T">Objeto que define al momento de llamar el método.</typeparam>
        /// <param name="obj">String que representa una objeto json para ser convertido.</param>
        /// <returns>Objeto que se obtiene desde un string.</returns>
        public static T ReadFromJsonWithSetting<T>(this string obj)
        {
            return JsonConvert.DeserializeObject<T>(obj, new Newtonsoft.Json.JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.Auto,
                NullValueHandling = NullValueHandling.Ignore,
            });
        }

        /// <summary>
        /// Convierte un string con formato Json a un objeto enviado.
        /// </summary>
        /// <typeparam name="T">Tipo de objeto que se espera retornar.</typeparam>
        /// <param name="jsonString">String con formato Json.</param>
        /// <returns>Objeto del tipo enviado.</returns>
        public static T ReadFromJson<T>(this string jsonString)
        {
            return JsonConvert.DeserializeObject<T>(jsonString);
        }

        /// <summary>
        /// Convierte un string con formato Json a un objeto enviado.
        /// </summary>
        /// <param name="jsonString">String con formato Json.</param>
        /// <param name="messageType"></param>
        /// <returns></returns>
        public static object ReadFromJson(this string jsonString, string messageType)
        {
            var type = Type.GetType(messageType);
            return JsonConvert.DeserializeObject(jsonString, type);
        }

        /// <summary>
        /// Convierte un string en formato Json en un objeto.
        /// </summary>
        /// <param name="jsonString">String en formato Json.</param>
        /// <returns>Objeto Json.</returns>
        public static object ReadFromJson(this string jsonString)
        {
            return JsonConvert.DeserializeObject(jsonString);
        }

        #endregion
    }
}
