﻿using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Routing;
using Tisal.Api.Servicio.Core;
using Tisal.Api.Triage.Controllers;
using Tisal.NCommon.ComponentModel;
using Unity;
using System.Collections.Generic;

namespace Tisal.Lyra.Urgencia.Api.Test
{
    [TestClass]
    public class TriageTest
    {
        #region Propiedades

        /// <summary>
        /// Contenedor para registrar los tipos.
        /// </summary>
        private IUnityContainer _container;

        /// <summary>
        /// Controlador de Triage.
        /// </summary>
        private TriageController _triageController;

        #endregion

        #region Inicializadores

        /// <summary>
        /// Iniciliza prueba unitaria del api.
        /// </summary>
        [TestInitialize]
        public void Initialize()
        {
            _container = new UnityContainer();
            CargadorModulo.CargarContenedor(_container, ".\\bin", "*.Inicializador.dll");
            CargadorMapeos.CargarPerfiles(_container, ".\\bin", "*.Servicio.dll");
            _triageController = new TriageController(_container.Resolve<ITriageServicio>());

            IConfigurationSource config = ConfigurationSourceFactory.Create();
            ExceptionPolicyFactory factory = new ExceptionPolicyFactory(config);
            try { Logger.SetLogWriter(new LogWriterFactory().Create()); }
            catch { }
            ExceptionManager exManager = factory.CreateManager();
            try { ExceptionPolicy.SetExceptionManager(factory.CreateManager()); }
            catch { }
        }

        #endregion

        #region Metodos de Prueba

        /// <summary>
        /// Método con prueba unitaria de repositorio para GetTriage.
        /// </summary>
        [TestMethod]
        public void TestGetTriage()
        {
            ConfigurarController("http://localhost/triage/api/v1/gettriage");

            IHttpActionResult response = _triageController.GetTriage(new GetTriageRequest()
            {
                CodigoEmpresa = 4,
                CodigoSucursal = 1,
                CodigoOrigen = 2,
                IdAtencion = 1109619,
                IdAtencionLyra = 2,
                VigenciaTriage = "S",
                VigenciaUsuario = "S"                
            });

            var content = response.ExecuteAsync(new System.Threading.CancellationToken());
            MetaResponseGetTriage metaResponse;
            // Evaluacion de condiciones exitosas
            Assert.IsTrue(content.Result.TryGetContentValue(out metaResponse));
            Assert.AreEqual("00", metaResponse.Header.Code);
        }

        /// <summary>
        /// Método con prueba unitaria de repositorio para GetTriage.
        /// </summary>
        [TestMethod]
        public void TestAddTriage()
        {
            ConfigurarController("http://localhost/triage/api/v1/addtriage");

            var listadoParametroClinico = new List<ParametroClinicoRequest>();
            listadoParametroClinico.Add(new ParametroClinicoRequest() { CodigoItem = 5, Valor = "5" });
            listadoParametroClinico.Add(new ParametroClinicoRequest() { CodigoItem = 6, Valor = "6" });

            IHttpActionResult response = _triageController.AddTriage(new AddTriageRequest()
            {
                CodigoEmpresa = 4,
                CodigoSucursal = 1,
                CodigoOrigen = 2,
                CodigoSeccion = 7,
                OrigenData = "T",
                IdAtencion = 1109618,
                IdAtencionLyra = 1,
                IdUsuario = "INDRA",
                CodigoClasificacion = 1,
                MotivoConsulta = string.Empty,
                DescripcionAlergia = string.Empty,
                DescripcionAntecendente = string.Empty,
                FechaInicioTriage = DateTime.Now,
                Vigencia = "S",
                AreaAtencion = "6",
                ListadoParametroClinico = listadoParametroClinico
            });

            var content = response.ExecuteAsync(new System.Threading.CancellationToken());
            MetaResponseAddTriage metaResponse;
            // Evaluacion de condiciones exitosas
            Assert.IsTrue(content.Result.TryGetContentValue(out metaResponse));
            Assert.AreEqual("00", metaResponse.Header.Code);
        }

        #endregion

        #region Private

        /// <summary>
        /// Configura el controlador.
        /// </summary>
        private void ConfigurarController(string uri)
        {
            _triageController.Request = new HttpRequestMessage
            {
                RequestUri = new Uri(uri)
            };

            _triageController.Configuration = new HttpConfiguration();
            _triageController.Configuration.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "paciente/api/v1/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional });

            _triageController.RequestContext.RouteData = new HttpRouteData(
                route: new HttpRoute(),
                values: new HttpRouteValueDictionary { { "Controller", "Triage" } });
        }

        #endregion
    }
}
